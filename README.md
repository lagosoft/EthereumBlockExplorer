# Ethereum Block Explorer

### A simple Ethereum block explorer to:
- List the last X blocks
- Inspect a block onMouseOver
- Show only transactions that involve Eth
- Built with React JS

