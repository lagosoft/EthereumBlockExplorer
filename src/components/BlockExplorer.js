import React, { Component } from 'react'
import axios from 'axios'

const blockCypherMainURL = 'https://api.blockcypher.com/v1/eth/main'
const blockCypherBlocksURL = 'https://api.blockcypher.com/v1/eth/main/blocks/'
const blockCypherTrxURL = 'https://api.blockcypher.com/v1/eth/main/txs/'

// https://www.blockcypher.com/dev/ethereum/

class BlockExplorer extends Component {
    state = {
        blockCount: 10,
        showFullHash: false,
        showFullPrevious: false,
    }

    componentWillMount() {
        axios.get(blockCypherMainURL)
            .then((resp) => {
                this.setState({
                    lastBlock: resp.data.hash,
                    lastHeight: resp.data.height
                })
            })
    }

    inspectBlock(height) {
        if (this.state.selectedBlock === height) {
            this.setState({
                selectedBlock: -1
            })
            return
        }

        if (this.state[height]) {
            this.setState({
                blockInfo: this.state[height],
                selectedBlock: height
            })
        } else {
            let url = blockCypherBlocksURL + height
            axios.get(url)
                .then((resp) => {
                    console.log("BLOCK DATA", resp.data);
                    this.setState({
                        [height]: resp.data,
                        blockInfo: resp.data,
                        selectedBlock: height
                    })
                })
        }
    }

    selectTransaction(trxId) {
        this.setState({ selectedTrx: null, selectedTrxInfo: null })
        const url = blockCypherTrxURL + trxId
        axios.get(url)
            .then((resp) => {
                console.log("DETAILS: ", resp)
                this.setState({
                    selectedTrx: trxId,
                    selectedTrxInfo: resp.data
                })
            })
    }

    reset() {
        this.setState({
            blockInfo: {},
            selectedBlock: -1,
            selectedTrx: '',
            selectedTrxInfo: {}
        })
    }

    getLatestBlocks() {

        let content = [];
        for (let i = this.state.lastHeight - 10; i < this.state.lastHeight; i++) {
            content.push(
                <div key={i}>
                    <div className="Block Clickable"
                        onClick={this.inspectBlock.bind(this, i)}>
                        {!this.state.lastBlock ? null : `${i}`}
                    </div>
                </div>
            )
        }

        const { blockInfo, showFullHash, showFullPrevious } = this.state
        return (
            <div className="Latest-blocks">
                <div className="Title">Last {this.state.blockCount} Block{this.state.blockCount > 1 ? 's' : null}:</div>
                <div className="Content">
                    <div className="Block-info">
                        <div className="Block-list">{content}</div>
                        <div className="Block-info-separator">&nbsp;</div>
                        {
                            blockInfo
                                ? <div className="Block-details">
                                    <div className="Block-details-row">
                                        <div>Chain</div>
                                        <code className="pullright">{blockInfo.chain}</code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Hash</div>
                                        <code className="pullright Clickable" onClick={() => this.setState({ showFullHash: !showFullHash })}>
                                            {showFullHash ? `${blockInfo.hash}` : `${blockInfo.hash.substring(0, 10)}...`}
                                        </code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Height</div>
                                        <code className="pullright">{blockInfo.height}</code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Nonce</div>
                                        <code className="pullright">{blockInfo.nonce}</code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Previous</div>
                                        <code className="pullright Clickable" onClick={() => this.setState({ showFullPrevious: !showFullPrevious })}>
                                            {showFullPrevious ? `${blockInfo.prev_block}` : `${blockInfo.prev_block.substring(0, 10)}...`}
                                        </code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Time</div>
                                        <code className="pullright">{blockInfo.time}</code>
                                    </div>
                                    <div className="Block-details-row">
                                        <div>Total</div>
                                        <code className="pullright">{blockInfo.total}</code>
                                    </div>
                                </div>
                                : <div className="Block-details" />
                        }
                    </div>
                </div>
            </div>
        )
    }

    getSelectedBlockTrxs() {
        let content = []

        if (this.state.blockInfo) {
            this.state.blockInfo.txids.forEach((trx, i) => {
                content.push(
                    <div key={i} onClick={this.selectTransaction.bind(this, trx)} className="Clickable">
                        <span>{trx}</span>
                    </div>)
            })
        }

        return (
            <div className="Block-transactions">
                <div className="Title">Transactions {this.state.blockInfo ? <span> for block: <code className="Color-block">{this.state.blockInfo.height}</code></span> : <span />}</div>
                <div className="Content">{content}</div>
            </div>
        )
    }

    getSelectedTrxDetails() {
        let content = []
        if (this.state.selectedTrx && this.state.selectedTrxInfo) {
            let trx = this.state.selectedTrxInfo
            content.push(
                <div className="Details-intro" key={this.state.selectedTrx}>
                    <div>
                        <div className="Details-row"><span>Transaction Id</span><code className="Color-trx">{this.state.selectedTrx}</code></div>
                        <div className="Details-row"><span>From</span><code>{trx.addresses[0]}</code></div>
                        <div className="Details-row"><span>To</span><code>{trx.addresses[1]}</code></div>
                        <div className="Details-row"><span>Total</span><code>{trx.total}</code></div>
                    </div>
                    <hr />
                    <div className="Details-row"><span>Cofirmed</span><code>{trx.confirmed}</code></div>
                    <div className="Details-row"><span>confirmations</span><code>{trx.confirmations}</code></div>
                    <div className="Details-row"><span>confidence</span><code>{trx.confidence}</code></div>
                    <div className="Details-row"><span>double spend</span><code>{trx.double_spend ? 'true' : 'false'}</code></div>
                    <hr />
                    <div className="Details-row"><span>fees</span><code>{trx.fees}</code></div>
                    <div className="Details-row"><span>gas used</span><code>{trx.gas_used}</code></div>
                    <div className="Details-row"><span>gas price</span><code>{trx.gas_price}</code></div>
                    <div className="Details-row"><span>gas limit</span><code>{trx.gas_limit}</code></div>
                    <div className="Details-row"><span>size</span><code>{trx.size}</code></div>
                </div>
            )
        }

        return (
            <div className="Transaction-details">
                <div className="Title">Transaction Details :</div>
                <div className="Content">{content}</div>
            </div>
        )
    }

    render() {
        return (
            <div className="Main-data">
                <div className="Blocks-and-trxs">
                    {this.getLatestBlocks()}
                    {this.getSelectedBlockTrxs()}
                </div>
                {this.getSelectedTrxDetails()}
            </div>
        )
    }
}

export default BlockExplorer