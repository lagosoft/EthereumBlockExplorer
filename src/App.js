import React, { Component } from 'react'

import BlockExplorer from './components/BlockExplorer'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="App-title">ETH Block <code className="Code-title">Explorer</code></div>
        </header>
        <BlockExplorer />
        <br />
      </div>
    )
  }
}

export default App


/**
 * DESCRIPTION
Build a small app to explore the latest blocks on Ethereum. 
The goal of the app is to provide a way to glance at the recent Ether transfers happening on the blockchain.

REQUIREMENTS
Display the ten latest blocks.
Allow the user to see the transactions from a block. Only the transactions sending Ether should be displayed.
Allow the user to see some details about a transaction.
NOTES
You can use any library you feel comfortable with.
The app will be loaded in a browser with MetaMask.
Feel free to interpret the requirements in any way that you think could be interesting.
The app doesn’t need to follow the Aragon visual identity.
Don’t hesitate to ask any question to the team :-)
THE SUBMISSION WILL BE EVALUATED ON THE FOLLOWING POINTS:
Code quality / readability / testability
UX / visual solutions
Perceived performance
Accessibility

 */